<?php session_start() ?>
<!DOCTYPE html>
<html>
    <head>
        <title>Wifi Hotspots</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="./css/styles.css">
        <!--Stylesheet necessary for the stars-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
        <link rel="shortcut icon" href="./images/wifi.ico" type="image/ico">
		<link rel="shortcut icon" href="./images/wifi.ico" type="image/ico">
		<link rel="apple-touch-icon-precomposed" href="./images/Wifi-Icon.jpg"/>
		<link rel="apple-touch-startup-image" href="https://www.servti.com/blog/wp-content/uploads/2018/04/free-wifi-Copy.jpg" />
		<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ==" crossorigin=""/>
		<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ==" crossorigin=""/>
		<script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js" integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw==" crossorigin=""></script>
		<meta property="og:title" content="CAB230 Wifi Hotspot Finder" />
		<meta property="og:type" content="website" />
		<meta property="og:url" content="https://cab230.sef.qut.edu.au/Students/n10003045/" />
		<meta property="og:image" content="./images/wifi.png" />
		<meta name="twitter:card" content="summary">
		<meta name="twitter:url" content="https://cab230.sef.qut.edu.au/Students/n10003045/"/>
		<meta name="twitter:title" content="CAB230 Wifi Hotspot Finder"/>
		<meta name="twitter:description" content="A WifiHotspot"/>
		<meta name="twitter:image" content="./images/wifi.png"/>
		<meta name="description" content="A WiFI hotspot" />
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<meta name="apple-mobile-web-app-status-bar-style" content="black" />
		<meta name="viewport" content = "width = device-width, initial-scale = 2.3, minimum-scale = 1, maximum-scale = 5" />
        <script src="./js/geosearch.js"></script>
	</head>

    <body>
        <div id="wrapper">
            <!-- Page header and functions include files -->
            <?php require_once "./includes/partials/header.php";
                  require './includes/functions.inc';
            ?>
            
            <div id="main-content">
                <div id="search-form1">
                    <form id="form" method="post" action="index.php">
                        <!--OPTIONS: Suburb - Name - Rating - User's Location -->
                        <!--Name search-->
                        <input type="search" name="search" placeholder="Search" value=<?php $value = isset($_POST['search']) ? $_POST['search'] : ''; echo $value?>>

                        <!--Suburb selection dropdown-->
                        <label>Suburb</label>
                        <?php require_once './includes/scripts/suburbs.inc'?>

                        <!--Rating search-->
                        <label>Rating</label>
                        <select id="rating" name="rating">
                            <?php
                            //Generate ratings options, and select whichever might have been already selected
                            for ($i = 0; $i < 6; $i++) {
                                $value = ($i == 0 ? 'Any' : $i);
                                if (isset($_POST['rating']) && $_POST['rating'] == $value) {
                                    $sel = 'selected';
                                } else {
                                    $sel = '';
                                }
                                echo "<option value='$value' $sel>$value</option>";
                            }
                            ?>

                        </select>

                        <!--Search button-->
                        <input type="submit" value="Search">
                        OR
                        <!--Geolocation search button-->
                        <input type="button" value="Search my current location!" onclick="getLocation();">
                    </form>
                </div>

                <!-- Status paragraph tag, updated with messages to inform user
                     if no items have been found -->
                <p id = "status"></p>

                <!-- Div containing map of all results -->
                <div id="map">
					<?php require_once "./includes/scripts/map.inc";?>
				</div>

                <!--Empty results to later be populated with results-->
                <div id="results" style="visibility: hidden">
                    <?php require_once "./includes/scripts/results.inc";?>
                </div>
            </div>

            <!-- PHP include file containing the remaining part of the page -->
            <?php require_once "./includes/partials/footer.php" ?>
