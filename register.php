<?php session_start() ?>
<!DOCTYPE html>
<html>
    <head>
        <title>Register</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="./css/styles.css">
        <link rel="shortcut icon" href="./images/wifi.ico" type="image/ico">
    </head>

    <body>
        <div id="wrapper">
            <!-- PHP include files containing the header, PHP functions, and the
                 register script -->
            <?php require_once "./includes/partials/header.php";
                  require_once "./includes/functions.inc";
                  require_once "./includes/scripts/register.inc";
            ?>

            <div id="main-content">
                <form id="form" action="register.php" method="post">
                    <!--Registration form contains textboxes for: firstname, surname,
                        email, password and password confirmation, alongwith descriptions
                        and error messages. On keydown in any textbox it hides the corresponding
                        error message-->
                    <p><b>Name:</b></p>
                    <input type="text" name="firstname" placeholder="First name" maxlength="20" onkeydown="hideError('fname-err')"
                    value="<?= $_POST['firstname'] ?? ''; ?>">
                    <input type="text" name="surname" placeholder="Last name" maxlength="20" onkeydown="hideError('surname-err')"
                    value="<?= $_POST['surname'] ?? ''; ?>">
                    <p class="err-msg" id="fname-err"><?php global $errors; disp_err($errors, 'First', 'fname-err') ?></p>
                    <p class="err-msg" id="surname-err"><?php global $errors; disp_err($errors, 'Last', 'surname-err') ?></p>

                    <br><br><p><b>Enter your email address:</b></p>
                    <input type="email" id="email" name="email" placeholder="Email@email.com" onkeydown="hideError('email-err')"
                    value="<?= $_POST['email'] ?? ''; ?>">
                    <p class="err-msg" id="email-err"><?php global $errors; disp_err($errors, 'Email', 'email-err') ?><br></p>

                    <p><b>Choose your password:</b></p>
                    <input type="password" name="password" placeholder="Password" onkeydown="hideError('pass-err')"
                    value="<?= $_POST['password'] ?? ''; ?>">
                    <p class="err-msg" id="pass-err"><?php global $errors; disp_err($errors, 'Password ', 'pass-err') ?><br></p>

                    <p><b>Confirm your password:</b></p>
                    <input type="password" name="conf_pass" placeholder="Password" onkeydown="hideError('confpass-err')"
                    value="<?= $_POST['conf_pass'] ?? ''; ?>">
                    <p class="err-msg" id="confpass-err"><?php global $errors; disp_err($errors, 'Passwords', 'confpass-err') ?><br></p>

                    <!--Register submit button-->
                    <input type="submit" value="Register" id="submit">

                    <!--Execute the input validation javascript-->
                    <!-- <script src="./js/validate.js"></script> -->
                </form>
            </div>

            <!-- PHP include file containing the remaining part of the page -->
            <?php require_once "./includes/partials/footer.php" ?>
