<?php

// Logout page, starts the session, destroys all variables for this session and
// redirects user to signin page
session_start();
session_destroy();
header("location: signin.php");

?>
