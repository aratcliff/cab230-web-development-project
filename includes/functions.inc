<?php

$errors = [];
//Add message to error array
function error_message(&$errors, $message) {
    array_push($errors, $message);
}

//If error contains keyword (email, first, last, password), display that error
function disp_err($errors, $location, $err_id) {
    foreach ($errors as $error) {
        if (stripos($error, $location) !== false) {
            echo $error;
            echo "<style> #$err_id { visibility: visible; } </style>";
        }
    }
}

//Display all reviews for an item
function disp_reviews() {
    global $database;
    //Select the first name, last name, review date, rating and review of each review
    $result = $database -> prepare ("SELECT m.memberFirstName, m.memberLastName, r.reviewDate, r.reviewText, r.reviewRating, i.itemName
                                    FROM reviews r, members m, items i
                                    WHERE r.memberID = m.memberID 
                                    AND r.itemID = :id
									AND i.itemID = :id;");
    $result->bindValue(':id', $_GET['i']);
    $result->execute();

    //Display each review given in the form given in the review include file,
    //if there are no reviews, inform the user that
    $rowCount = $result->rowCount();
    if ($rowCount != 0) {
        foreach ($result as $review) {
            include './includes/partials/review.php';
        }
    } else {
        echo "No reviews have been submitted for this hotspot, be the first to do so.";
    }
}

//Get the width of the ratings bars
function get_ratings($rating) {
    //Get the number of reviews which match the given rating
    global $database, $reviewCount;
    $result = $database -> prepare("SELECT count(itemID) AS itemCount
              FROM reviews
              WHERE itemID = :id
              AND reviewRating = $rating;");
    $result->bindValue(':id', $_GET['i']);
    $result->execute();
    $value = $result -> fetch();

    //Calculate the width percentage of the bar
    if ($reviewCount > 0) {
        $percent = 100 - (($value['itemCount']/$reviewCount)*100);
        echo 'style="width:'.$percent.'%"';
    } else {
        echo 'style="width:100%"';
    }
}

//Get the average rating of a hotspot
function get_average($item) {
    global $database;
    $result = $database -> prepare("SELECT AVG(reviewRating) AS averageRating
                                  FROM reviews
                                  WHERE itemID = :id");
    $result->bindValue(':id', $item);
    $result->execute();
    $value = $result -> fetch();

    //Round up to nearest 0.5, or if it is already at 0.5 multiple, keep it there
    //EG 3.5 returns 3.5, 4 returns 4, 2.743 returns 3, 4.1 returns 4.5
    //Note: values multiplied by 10 before rounding, then divided by 10 again after rounding
    //This is to avoid a dividing by 0 error
    $n = $value['averageRating'] * 10;
    $x = 5;
    $rounded = (round($n)%$x === 0) ? round($n) : round(($n+$x/2)/$x)*$x;
    return $rounded / 10;
}

//Display the appropriate amount of stars for a given hotspot based on its average rating
function draw_stars($item) {
    //Get the average rating
    $avg = get_average($item);
    //If the average is 0, then no ratings have been given, inform the user
    if ($avg == 0) {
        echo '<i>No ratings</i>';
    } else {
        //Draw stars
        for ($i = 1; $i < $avg; $i++) {
            echo "<span class='fa fa-star checked'></span>";
        }
        if (($avg * 2) % 2 == 0) {
            echo "<span class='fa fa-star checked'></span>";
        } else {
            echo "<span class='fa fa-star-half half'></span>";
        }
    }
}

//Displays a map centered on given latitude and longitude coordinates
//Map is using Leaflet JavaScript libray
function display_map($lat, $lon, $name, $map) {
    //Add the map
    echo '<script>
			var mymap = L.map("'.$map.'").setView(['.$lat.', '.$lon.'], 14);

			L.tileLayer("https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiY21hYzE5OTkiLCJhIjoiY2poaWcwdWYyMjZoZzM2bm13ZWV2ODByMyJ9.LsODyuBK67nNc2Y-vW58jg", {
				maxZoom: 18,
				attribution: "Map data &copy; OpenStreetMap contributors, " +
					"CC-BY-SA, " +
					"Imagery © Mapbox",
				id: "mapbox.streets"
			}).addTo(mymap);';

    //Add popup marker if told to. Told to on wifi hotspot pages, told not to on results page
    if ($name == '') {
        echo '</script>';
    } else {
        echo 'var marker = L.marker(['.$lat.', '.$lon.']).addTo(mymap);
        marker.bindPopup("'.$name.'");
        </script>';
    }
}

?>
