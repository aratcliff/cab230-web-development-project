<?php

//Include file to get results of a given query
require './config/DBConfig.inc';

$pdo = new PDO("mysql:host=localhost;dbname=$dbName", $username, $password);
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

try {
    $result = $pdo -> query($query);
} catch (PDOException $e) {
    echo $e -> getMessage();
}

?>
