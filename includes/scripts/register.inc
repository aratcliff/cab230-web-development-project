<?php
//Check a string only has letters and white space in it.
function char_only($string, $location) {
    global $errors;
    if (!preg_match("/^[a-zA-Z ]*$/", $string)) {
        error_message($errors, "Only letters and white space allowed in the $location");
        return false;
    }
    return true;
}

//Check a string is longer than a certain length
function check_length($string, $length, $location) {
    global $errors;
    if (strlen($string) >= $length) {
        return true;
    } else {
        error_message($errors, "$location must be at least $length characters long");
        return false;
    }
}

//Check if input email is of email format, then check if it is in the database already
function validate_email($email) {
    global $errors;
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        error_message($errors, "Invalid email format");
        return false;
    } else {
        //Query to get emails
        $query = "SELECT memberEmail FROM members";
        require './includes/scripts/pdo.inc';
        //Check if it is in the database
        $count = 0;
        foreach ($result as $member) {
            if ($member['memberEmail'] == $email) {
                error_message($errors, "Email already exists, please choose a different email");
                return false;
                break;
            } else {
                return true;
            }
            $count++;
        }
        if ($count == 0) {
            return true;
        }
    }
}

//Validate the password, it needs to be longer than 8 characters and match the
//given password with the given confirmation password
function validate_password($password, $confpass) {
    global $errors;
    if (check_length($password, 8, 'Password')) {
        if ($password == $confpass) {
            return true;
        } else {
            error_message($errors, "Passwords do not match");
            return false;
        }
    } else {
        error_message($errors, "Passwords must match and be longer than 8 characters");
        return false;
    }
}

//Validate all given data
function validate_data() {
    if (isset($_POST['firstname']) || isset($_POST['surname']) || isset($_POST['email']) || isset($_POST['password']) || isset($_POST['conf_pass'])) {
        //Set up boolean values for each input
        $fname = false;
        $lname = false;
        $email = validate_email($_POST['email']);
        $password = validate_password($_POST['password'], $_POST['conf_pass']);

        //Validate first name
        if (check_length($_POST['firstname'], 2, 'First name') && char_only($_POST['firstname'], 'First name')) {
            $fname = true;
        }
        //Validate surname
        if (check_length($_POST['surname'], 2, 'Last name') && char_only($_POST['surname'], 'Last name')) {
            $lname = true;
        }

        if ($fname && $lname && $email && $password) {
            //Create new user accounts
            try {
                require "./config/DBConfig.inc";
                global $database;
                $sql = "USE $dbName;
                        INSERT IGNORE INTO members (memberFirstName, memberLastName, memberEmail, memberPassword, salt)
                        VALUES (:fname, :lname, :email, :password, :salt)";
                $stmt = $database -> prepare($sql);

                $salt = substr(md5(uniqid()), 0, 20);
                $password = $_POST['password'] . $salt;

                $array = array(
                    ":fname" => $_POST['firstname'],
                    ":lname" => $_POST['surname'],
                    ":email" => $_POST['email'],
                    ":password" => sha1($password),
                    ":salt" => $salt
                );

                $stmt -> execute($array);

                header('Location: index.php');
            } catch (PDOException $e) {
                echo $e->getMessage();
            }
        } else {
            global $errors;
        }
    }
}

validate_data();

?>
