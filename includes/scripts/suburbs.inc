<?php
//Create array of all DISTINCT SUBURBS
$query = "SELECT DISTINCT itemSuburb FROM items ORDER BY itemSuburb;";
require './includes/scripts/pdo.inc';

//Remove ', [POSTCODE]' from suburbs
$suburbs = array('Any');
foreach ($result as $item) {
    $suburb = preg_replace('/,\s[0-9]+/', '', $item['itemSuburb']);
    if (!in_array($suburb, $suburbs)) {
        $suburbs[] = $suburb;
    }
}

//Create select html form element with each suburb as an option
echo "<select id='suburb' name='suburb'>";
$selected;
for ($i = 0; $i < count($suburbs); $i++) {
    //Sets the selected option if it already has been set
    if (isset($_POST['suburb']) && $_POST['suburb']==$suburbs[$i]) {
        $selected = 'selected="selected"';
    } else {
        $selected = '';
    }
    echo "<option $selected value='$suburbs[$i]'>$suburbs[$i]</option>";
}
echo '</select>';

?>
