<?php

//If search has been performed via search, suburb or rating options, get the results
if (isset($_POST['search']) && isset($_POST['suburb']) && isset($_POST['rating'])) {
    //SEARCH VALUE
    if (strlen($_POST['search']) > 0) {
        $search = "'%".$_POST['search']."%'";
    } else {
        $search = "'%'";
    }

    //MATCH SUBURB VALUE
    if ($_POST['suburb'] == 'Any') {
        $suburb = "'%'";
    } else {
        $suburb = "'".$_POST['suburb']."%'";
    }

    //CHECK RATINGS VALUE
    if ($_POST['rating'] == 'Any') {
        $query = "SELECT itemID, itemName, itemAddress, itemSuburb, itemImgLink, itemLatitude, itemLongitude FROM items
        WHERE (itemName LIKE ".$search." OR itemAddress LIKE ".$search." OR itemSuburb LIKE ".$search.") AND itemSuburb LIKE $suburb";
    } else {
        $query = "SELECT i.itemID, i.itemName, i.itemAddress, i.itemSuburb, i.itemImgLink, avg(r.reviewRating), i.itemLatitude, i.itemLongitude
                FROM items i, reviews r
                WHERE i.itemID = r.itemID
                AND (itemName LIKE ".$search." OR itemAddress LIKE ".$search." OR itemSuburb LIKE ".$search.") AND itemSuburb LIKE $suburb
                GROUP BY r.itemID
                HAVING avg(r.reviewRating) >= ".$_POST['rating'];
    }

    disp_results($query);
}

//If search requested by location, get results as follows
if (isset($_GET['lat']) && isset($_GET['lon'])) {
    //Get the latitude and longitude from url and convert to radians
    $lat = deg2rad(htmlspecialchars($_GET['lat']));
    $lon = deg2rad(htmlspecialchars($_GET['lon']));

    //Calculate bounding area - as a square - of latitude and longitude
    //Sides are 5km from given point
    //Distance (km) / Approximate radius of Earth (km)
    $r = (5/6371);
    //calculate min and max latitudes in radians, and convert to degrees
    $latmin = rad2deg($lat - $r);
    $latmax = rad2deg($lat + $r);
    //calculate min and max longitudes
    $deltaLon = asin(sin($r)/cos($lat));
    $lonmin = rad2deg($lon - $deltaLon);
    $lonmax = rad2deg($lon + $deltaLon);

    //SQL query to return results based on user location
    $query = "SELECT itemID, itemName, itemAddress, itemSuburb, itemImgLink, itemLatitude, itemLongitude FROM items
              WHERE itemLatitude BETWEEN $latmin AND $latmax
              AND itemLongitude BETWEEN $lonmin AND $lonmax;";

    disp_results($query);
}

//Display results
function disp_results($query) {
    //Adjust CSS to show map and results
    echo '<script>
    //Move search form to top and make results div visible
    form.style.top = 0;
    form.style.left = "50%";
    form.style.transform = "translate(-50%, 20px)";
    document.getElementById("results").style.visibility = "visible";
	document.getElementById("map").style.visibility = "visible";
    </script>';

    //Get results from query
    require './includes/scripts/pdo.inc';

    //Check number of results
	$noOfRows = $result->rowCount();
    //If there are results display a map centered on the first result
	if ($noOfRows != 0) {
		$lat = getFirstLatLon($query)[0];
		$lon = getFirstLatLon($query)[1];
        display_map($lat, $lon, '', 'map');

        //Add markers and display links to each hotspot page
        foreach ($result as $item) {
            include './includes/partials/result.php';
            $current_lat = $item['itemLatitude'];
            $current_lon = $item['itemLongitude'];
            $current_name = $item['itemName'];
            $current_id = $item['itemID'];
            $link = "<a href='./item.php?i=$current_id'> $current_name </a>";
            echo '<script>
            var marker = L.marker(['.$current_lat.', '.$current_lon.']).addTo(mymap);
            marker.bindPopup("'.$link.'");
            </script>';
        }
	} else {
        //If there are no results, display map centered on Brisbane CBD
        display_map(-27.472831442, 153.023499906, '', 'map');

        //Tell user there search query has no results
        echo 'Sorry, there are no results that match the search conditions, please try something else.';
	}
}

//Get the latitude and longitude of the first result
function getFirstLatLon($query) {
    global $database;
    require './includes/scripts/pdo.inc';

    $first_entry = $result -> fetch();
    $lat = $first_entry['itemLatitude'];
    $lon = $first_entry['itemLongitude'];

    return array($lat, $lon);
}

?>
