<?php

//Add a review
function add_review($rating) {
    global $database;

    //Add review to database
    $sql = "INSERT IGNORE INTO reviews (itemID, memberID, reviewDate, reviewRating, reviewText)
            VALUES (:item, :user, :reviewDate, :rating, :review)";
    $stmt = $database->prepare($sql);
    $array =  array(
        ":item" => $_GET['i'],
        ":user" => $_SESSION['userID'],
        ":reviewDate" => date('Y-m-d'),
        ":rating" => $rating,
        ":review" => $_POST['review']
    );

    $stmt->execute($array);
}

//Connect to database
require './config/DBConfig.inc';
try {
    $database = new PDO("mysql:host=$host;dbname=$dbName", $username, $password);
    $database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

} catch (PDOException $e) {
    echo $e->getMessage();
}

if (isset($_POST['user_rating'])) {
    //User has submitted a rating
    if (isset($_SESSION['loggedIn'])) {
        //User is logged in
            $rating = $_POST['user_rating'];
        if ($rating == 1 || $rating == 2 || $rating == 3 || $rating == 4 || $rating == 5) {
            //Rating is either 1, 2, 3, 4, or 5
            add_review($rating);
        } else {
            //Rating isn't an allowed value, round it to nearest value.
            if ($rating > 5) {
                $rating = 5;
            } else if ($rating < 1) {
                $rating = 1;
            } else {
                $rating = round($rating, 0);
            }
            add_review($rating);
        }


    } else {
        //User is not logged in
        //Set cookie data so review can be added once the user logs in
        setcookie('rating', $_POST['user_rating'], time() + (60 * 20), "/");
        setcookie('review', $_POST['review'], time() + (60 * 20), "/");
        //Redirect to login page
        header ('Location: ./signin.php?i='.$_GET['i']);
    }
}

?>
