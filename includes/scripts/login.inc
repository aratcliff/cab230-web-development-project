<?php

//If user has entered either a password or email, check they have valid credentials
if (isset($_POST['email']) || isset($_POST['password'])) {
    global $errors;

    //Get user details which match the email entered by the user
    $query = "SELECT memberID, memberPassword, salt FROM members WHERE '".$_POST['email']."' = memberEmail";
    try {
        $result = $database -> query($query) -> fetch();
    } catch (PDOException $e) {
        echo $e -> getMessage();
    }

    //Check if encrypting the posted password with the salt matching the email matches the users password
    $password = sha1($_POST['password'] . $result['salt']);
    if ($result['memberPassword'] == $password) {
        //If so, login
        logIn($result['memberID']);
    } else {
        //If not, display error message
        error_message($errors, "Either your password or email are incorrect");
    }
}

//Login function
function logIn($memberID) {
    //Get database variable within this function
    global $database;

    if (isset($_GET['i'])) {
        //Add users review to database if thats what sent them to login page
        $sql = "INSERT IGNORE INTO reviews (itemID, memberID, reviewDate, reviewRating, reviewText)
                VALUES (:item, :user, :reviewDate, :rating, :review)";
        $stmt = $database -> prepare($sql);
        $array =  array(
            ":item" => $_GET['i'],
            ":user" => $memberID,
            ":reviewDate" => date('Y-m-d'),
            ":rating" => $_COOKIE['rating'],
            ":review" => $_COOKIE['review']
        );

        $stmt -> execute($array);

        //Log user in and redirect them back to the item page they came from
        $_SESSION['loggedIn'] = true;
        $_SESSION['userID'] = $memberID;
        header('Location: ./item.php?i='.$_GET['i']);
    } else {
        //Log user in and send them to home page
        $_SESSION['loggedIn'] = true;
        $_SESSION['userID'] = $memberID;
        header('Location: ./');
    }
}



?>
