<!-- Div that displays results, included once per result that shows up -->
<div class="item">
    <img src="<?php echo ($item['itemImgLink'] != '' ? htmlspecialchars($item['itemImgLink']) : './images/wifi.ico') ?>"
         alt="<?php $item['itemName'] ?>">
    <div class="links">
        <h3 style="margin: 10px auto;"><a href='./item.php?i=<?php echo $item['itemID'] ?>'><?php echo $item['itemName'] ?><span></span></a></h3>
    </div>
    <?php draw_stars($item['itemID']); ?>
    <p style='font-size: 0.8em; margin:5px auto;'><?php echo $item['itemAddress']?></p>
    <p style='font-size: 0.8em; margin:5px auto;'><?php echo $item['itemSuburb']?></p>
</div>
