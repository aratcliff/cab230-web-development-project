<!-- Div which contains reviews, included once per review for each hotspot review -->
<div itemprop="Review" itemscope itemtype="http://schema.org/Review" class='review'>
    <span itemscope itemtype="http://schema.org/Rating">
    <b itemprop="ratingValue"><?php echo $review['reviewRating'] ?></b></span> | <b itemprop="author"><?php echo htmlspecialchars($review['memberFirstName']).' '.htmlspecialchars($review['memberLastName']) ?></b> <i itemprop="datePublished"><?php echo $review['reviewDate'] ?></i>
    <hr>
    <p itemprop="description"><?php echo ($review['reviewText'] != '' ? htmlspecialchars($review['reviewText']) : '<em>No review given.</em>') ?></p>
	<div style="visibility: hidden"> <p itemprop="itemReviewed"><?php echo $review['itemName'] ?></p></div>
</div>
