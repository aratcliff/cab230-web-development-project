<!--Header div, contains links to sign in/register and the home page-->
<?php require_once './database.php'; ?>
<div id="header">
    <a href="./"><h1>CAB230 Project Wifi Hotspot</h1></a>
    <!-- Determine whether to display Login|Register or Logout links -->
    <?php if (!isset($_SESSION['loggedIn'])) { ?>
        <p><a href="./signin.php" class="headlink">Sign In</a> | <a href="./register.php" class="headlink">Register</a></p>
    <?php } else { ?>
        <p><a href="./logout.php" class="headlink">Log Out</a></p>
    <?php } ?>
</div>
