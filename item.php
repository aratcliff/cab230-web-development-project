<?php
//Start the session and add a review if one has been given
session_start();
require_once './includes/scripts/addreview.inc';
?>
<!DOCTYPE html>
<html>
    <head>
		<?php $current_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>
        <title>Wifi Hotspot</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="./css/styles.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
        <link rel="shortcut icon" href="./images/wifi.ico" type="image/ico">
		<link rel="apple-touch-icon-precomposed" href="./images/wifi.ico"/>
		<link rel="apple-touch-startup-image" href="https://www.servti.com/blog/wp-content/uploads/2018/04/free-wifi-Copy.jpg" />
		<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ==" crossorigin=""/>
		<script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js" integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw==" crossorigin=""></script>
		<meta property="og:title" content="CAB230 Wifi Hotspot Finder" />
		<meta property="og:type" content="website" />
		<meta property="og:url" content="<?php echo $current_link; ?>" />
		<meta property="og:image" content="./images/wifi.png" />
		<meta name="twitter:card" content="summary">
		<meta name="twitter:url" content="<?php echo $current_link; ?>"/>
		<meta name="twitter:title" content="CAB230 Wifi Hotspot Finder"/>
		<meta name="twitter:description" content="A WifiHotspot"/>
		<meta name="twitter:image" content="./images/wifi.png"/>
		<meta name="description" content="A WiFI hotspot" />
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<meta name="apple-mobile-web-app-status-bar-style" content="black" />
		<meta name="viewport" content = "width = device-width, initial-scale = 2.3, minimum-scale = 1, maximum-scale = 5" />
    </head>

    <body>
        <div id="wrapper">
            <!-- PHP include files for the header of the file, and the PHP
                 general functions-->
            <?php require_once "./includes/partials/header.php";
                require './includes/functions.inc';

                //Get most of the information for this hotspot
                $query = "SELECT * FROM items WHERE itemID = '".$_GET['i']."'";
                require './includes/scripts/pdo.inc';
                foreach ($result as $item) {
                    $name = $item['itemName'];
                    $address = $item['itemAddress'];
                    $suburb = $item['itemSuburb'];
                    $imglink = $item['itemImgLink'];
					$lat = $item['itemLatitude'];
					$lon = $item['itemLongitude'];
                }
            ?>

            <div id="main-content">
                <div id='reviews'>
                    <!--Displays the data of the wifi hotspot, image, name, address and star rating-->
                    <div id="item" itemscope itemtype="http://schema.org/Place">
    					<img itemprop="image" src="<?php echo ($imglink != '' ? htmlspecialchars($imglink) : './images/wifi.ico') ?>"
         alt="<?php echo $name ?>" style="width:300px;height:300px;"/>
    					<h1 itemprop="name"> <?php echo $name ?> </h1>
						<div id="addressMicrodata" itemscope itemtype="http://schema.org/PostalAddress">
							<h3 itemprop="streetAddress"> <?php echo $address ?> </h3>
							<h3 itemprop="addressLocality"> <?php echo $suburb ?> </h3>
						</div>
    					<br>
    					<br>
                        <!-- Uses the draw_stars() PHP function to display the
                             corect amount of stars for this hotspot-->
    					<?php draw_stars($_GET['i']); ?>
                        <!-- Displays the average rating of hotspot -->
    					<h5> Rated <?php echo get_average($_GET['i']) ?> out of 5 stars (<?php
                        // Gets total amount of reviews for this item
                        $result = $database->prepare("SELECT count(itemID) AS itemCount
                                                    FROM reviews
                                                    WHERE itemID = :id");
                        $result->bindValue(':id', $_GET['i']);
                        $result->execute();
                        $value = $result->fetch();
                        $reviewCount = $value['itemCount'];
                        echo $reviewCount; ?> reviews) </h5>
    				</div>

                    <div class="ratings">
                        <!--Ratings div shows the overall ratings of this hotspot
                            the bars display the percentage of the ratings that were
                            given that rating-->
        				<div id="ratings">
                            <hr>
                            <h4>Ratings:</h4>
                            <hr>
        					<div class="star_rating">
        						<h6> Excellent (5) </h6>
        					</div>
        					<div class="bar1">
        						<div class="bar2" <?php get_ratings(5) ?>></div>
        					</div>
        					<br>
        					<div class="star_rating">
        						<h6> Very Good (4) </h6>
        					</div>
        					<div class="bar1">
        						<div class="bar2" <?php get_ratings(4) ?>></div>
        					</div>
        					<br>
        					<div class="star_rating">
        						<h6> Average (3) </h6>
        					</div>
        					<div class="bar1">
        						<div class="bar2" <?php get_ratings(3) ?>></div>
        					</div>
        					<br>
        					<div class="star_rating">
        						<h6> Poor (2) </h6>
        					</div>
        					<div class="bar1">
        						<div class="bar2" <?php get_ratings(2) ?>></div>
        					</div>
        					<br>
        					<div class="star_rating">
        						<h6> Terrible (1) </h6>
        					</div>
        					<div class="bar1">
        						<div class="bar2" <?php get_ratings(1) ?>></div>
        					</div>
        				</div>

                        <!--Div to display the reviews information-->
                        <!--Ratings form gives user to submit the rating out of 5 for the hotspot,
                            and if they want, leave a text review-->
    					<form id="ratings-form" method="post" action="<?php $_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'] ?>">
                            <hr>
                            <h4 style="text-align:center;"> Add your review! </h4>
                            <hr>
                            <label style="margin-left: 5%;"> Your Rating: </label>
    						<select id="rating" name="user_rating" style="width: 40%; margin-left: 15%;">
    							<option value="1">1</option>
    							<option value="2">2</option>
    							<option value="3">3</option>
    							<option value="4">4</option>
    							<option value="5">5</option>
    						</select>
    						<br>
    						<br>
    						<textarea name="review" cols="35" rows="5" placeholder="Enter your full review here (optional)&#13; Maximum 250 characters" maxlength="250"></textarea>
    						<br>
    						<input type="submit" value="Submit">
    					</form>
                    </div>

                    <!-- Div containing map of where wifi hotspot is, generated
                         from display_map() function in the function.inc include file -->
                    <div id="item-map">
                        <?php display_map($lat, $lon, $name, 'item-map'); ?>
                    </div>

                    <!-- Div containing list of reviews, generates list from
                         disp_reviews() function in the function.inc include file -->
                    <div id="review-list">
                        <h4>Reviews:</h4>
                        <?php disp_reviews(); ?>
                    </div>
                </div>
            </div>

            <!-- PHP include file containing the remaining part of the page -->
            <?php require_once "./includes/partials/footer.php" ?>
