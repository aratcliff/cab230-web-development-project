<?php

$time = microtime(TRUE);
$mem = memory_get_usage();

require './config/DBConfig.inc';

// Create a new database
try {
    $database = new PDO("mysql:host=$host;dbname=", $username, $password);
    $database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $sql = "CREATE DATABASE IF NOT EXISTS $dbName CHARACTER SET utf8 COLLATE utf8_general_ci;";

    $statement = $database->prepare($sql);
    $statement->execute();

} catch (PDOException $e) {
    echo $e->getMessage();
}
// Establish connection with newly created database
try {
    $database = new PDO("mysql:host=$host;dbname=$dbName", $username, $password);
    $database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

} catch (PDOException $e) {
    echo $e->getMessage();
}

// Create the users table
try {
    $sql = "CREATE TABLE IF NOT EXISTS members
                (memberID INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                memberFirstName VARCHAR(35) NOT NULL,
                memberLastName VARCHAR(35) NOT NULL,
                memberEmail VARCHAR(255) NOT NULL,
                memberPassword VARCHAR(255) NOT NULL,
                salt VARCHAR(20) NOT NULL)
                CHARACTER SET utf8 COLLATE utf8_general_ci;";

    $statement = $database->prepare($sql);
    $statement->execute();

} catch (PDOException $e) {
    echo $e->getMessage();
}

// Create the items table
try {
    $sql = "CREATE TABLE IF NOT EXISTS items
                (itemID INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                itemName VARCHAR(65) NOT NULL,
                itemAddress VARCHAR(150) NOT NULL,
                itemSuburb VARCHAR(50) NOT NULL,
                itemLatitude DECIMAL(10, 8) NOT NULL,
                itemLongitude DECIMAL(11, 8) NOT NULL)
                CHARACTER SET utf8 COLLATE utf8_general_ci;";

    $statement = $database->prepare($sql);
    $statement->execute();

} catch (PDOException $e) {
    echo $e->getMessage();
}

// Create the reviews table
try {
    $sql = "CREATE TABLE IF NOT EXISTS reviews
                (reviewID INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                itemID INT(6) NOT NULL,
                memberID INT(6) NOT NULL,
                reviewDate DATE NOT NULL,
                reviewRating ENUM('1', '2', '3', '4', '5') NOT NULL,
                reviewText TEXT)
                CHARACTER SET utf8 COLLATE utf8_general_ci;";

    $statement = $database->prepare($sql);
    $statement->execute();

} catch (PDOException $e) {
    echo $e->getMessage();
}

//Insert items from csv file into table IF TABLE IS EMPTY
$query = "SELECT COUNT(*) FROM items";
require './includes/scripts/pdo.inc';
foreach ($result as $item) {
if ((int)$item['COUNT(*)']==0) {#Table is not populated, so populate
    try {
        $sql = "LOAD DATA INFILE '../../htdocs/CAB230/stage2_submission/CSVData/CAB230-2018-Dataset-wifi-hot-spots-win.csv'
                INTO TABLE items
                COLUMNS TERMINATED BY ','
                OPTIONALLY ENCLOSED BY '\"'
                ESCAPED BY '\"'
                LINES TERMINATED BY '\n'
                IGNORE 1 LINES
                (itemName, itemAddress, itemSuburb, itemLatitude, itemLongitude);";

        $statement = $database->prepare($sql);
        $statement->execute();
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}}
?>
