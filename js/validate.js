var form = document.getElementById('form');

//Add event listener
form.addEventListener('submit', function(e) {
    e.preventDefault();
    //Create vars
    var fname = form['firstname'].value;
    var sname = form['surname'].value;
    var email = form['email'].value;
    var password = form['password'].value;
    var confpass = form['conf_pass'].value;

    //Validate each entry
    var boolfirstname = checkFirstname(fname);
    var boolsurname = checkSurname(sname);
    var boolemail = checkEmail(email);
    var boolpass = checkPass(password);
    var boolcpass = checkCPass(password, confpass);

    //Submit form if all entries are valid
    if (boolfirstname && boolsurname && boolemail && boolpass && boolcpass) {
        window.alert('Form submitted succesfully');
        window.location.pathname=window.location.pathname.replace("register.php", "index.php");
    }
});

//Validate the first name field
function checkFirstname(fname) {
    //It must exist and be longer than 1 character
     if (fname.length < 2) {
         document.getElementById('fname-err').style.visibility = "visible";
         return false;
     } else {
         return true;
     }
}

//Validate the surname field
function checkSurname(sname) {
    //It must exist and be longer than 1 character
     if (sname.length < 2) {
         document.getElementById('surname-err').style.visibility = "visible";
         return false;
     } else {
         return true;
     }
}

//Validate the email field
function checkEmail(email) {
    //The field must be entered, and comprise of the form:
    //[something@something.something]
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (re.test(String(email).toLowerCase())) {
        //email is valid
        return true;
    }
    else {
        document.getElementById('email-err').style.visibility = "visible";
        return false;
    }
}

//Validate the password
//Passwords should be at least 8 characters
function checkPass(password) {
    if (password.length < 8) {
        document.getElementById('pass-err').style.visibility = "visible";
        return false;
    } else {
        return true;
    }
}

function checkCPass(password, confpass) {
    //Check passwords are the same
    if (password != confpass) {
        document.getElementById('confpass-err').style.visibility = "visible";
        return false;
    } else {
        return true;
    }
}

//Hide error message upon key press
function hideError(element) {
    document.getElementById(element).style.visibility = "hidden";
}
