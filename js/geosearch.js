
function getLocation() {
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(showPosition, showError);
    } else {
		document.getElementById("reslts").innerHTML="Geolocation is not supported by this browser.";
        adjustCSS();
	}
}

//Change the URL to include latitude and longitude to be retrievable via PHP $_GET method
function showPosition(position) {
    location.replace(window.location.pathname + "?lat=" + position.coords.latitude + "&lon=" + position.coords.longitude);
}

//Show error if one happened while getting the user's location
function showError(error) {
	var msg = "";
	switch(error.code) {
		case error.PERMISSION_DENIED:
			msg = "User denied the request for Geolocation."
			break;
		case error.POSITION_UNAVAILABLE:
			msg = "Location information is unavailable."
			break;
		case error.TIMEOUT:
			msg = "The request to get user location timed out."
			break;
		case error.UNKNOWN_ERROR:
			msg = "An unknown error occurred."
			break;
	}
	document.getElementById("results").innerHTML = msg;
    adjustCSS();
}

//Adjust the CSS to show results
function adjustCSS() {
    var form = document.getElementById('form');
    form.style.top = 0;
    form.style.left = "50%";
    form.style.transform = "translate(-50%, 20px)";
    document.getElementById('results').style.visibility = 'visible';
}
