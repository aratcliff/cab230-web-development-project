# QUT CAB230 Project

Web application project developed for assessment.

The application is a web site which gives users the ability to review provided public Wi-Fi hotspots. Users can search based on a given text input, rating, suburb, or based on their location.

**Once imported to your server, adjust values in './config/DBConfig.inc' to match your SQL server's credentials.**

Project was completed in pairs. 
####The following was developed by me (Alexander Ratcliff):
*  Overall design of each page
*  Core functionality
    *  Item 3a
    *  Create account
    *  Log in
    *  Log out
    *  Search
    *  Leave a review
*  Mobile design

####The following was developed by my partner (Craig Macleod):
*  Data to support adding app to home screen of mobile phone
*  Maps and the map markers
*  Meta data