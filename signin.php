<?php session_start() ?>
<!DOCTYPE html>
<html>
    <head>
        <title>Sign In</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="./css/styles.css">
        <link rel="shortcut icon" href="./images/wifi.ico" type="image/ico">
    </head>

    <body>
        <div id="wrapper">
            <!-- PHP include files containing the header, PHP functions, and the
                 login script -->
            <?php require_once "./includes/partials/header.php";
                  require_once "./includes/functions.inc";
                  require_once "./includes/scripts/login.inc";
            ?>

            <div id="main-content">
                <!--Sign in form, contains textboxes for email and password,
                    alongwith descriptions of both entries-->
                <form id="form" action="<?php $_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'] ?>" method="post">
                    <h3>Sign in with your email:</h3>
                    <input type="email" id="email" name="email" placeholder="Email">
                    <br><br>
                    <input type="password" name="password" placeholder="Password">
                    <br><br>
                    <p class="err-msg" id="login-err"><?php global $errors; disp_err($errors, 'password', 'login-err') ?></p>
                    <br>
                    <!--Sign in form submit button-->
                    <input type="submit" value="Sign In" id="submit">
                </form>
            </div>

            <!-- PHP include file containing the remaining part of the page -->
            <?php require_once "./includes/partials/footer.php" ?>
